FROM alpine:3.14

CMD ["alembic", "upgrade", "heads"]

ENV PYTHONUNBUFFERED=1
RUN apk add --update --no-cache python3
RUN python3 -m ensurepip
RUN pip3 install --no-cache --upgrade pip setuptools
RUN pip3 install alembic GeoAlchemy2 psycopg2-binary

WORKDIR /data